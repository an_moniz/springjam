﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeightCounter : MonoBehaviour
{
    [SerializeField]
    public TMPro.TextMeshProUGUI score;

    public bool IsActive = false;

    private float height = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Reset()
    {
        height = 0;
        score.text = $"Score: {Mathf.Floor(height)}";
        IsActive = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (IsActive)
        {
            height = height + 1 * Time.deltaTime;
            score.text = $"Score: {Mathf.Floor(height)}";
        }
    }

    public float GetHeight()
    {
        return Mathf.Floor(height);
    }
}
