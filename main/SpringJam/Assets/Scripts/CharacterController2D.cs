﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class CharacterController2D : MonoBehaviour
{
    [SerializeField]
    private float _maxSpeed = 1.2f;
    [SerializeField]
    private float _jumpHeight = 6.5f;

    [SerializeField] private float _footRayCastLength = 0.08f;
    [SerializeField] private float _forwardRayCastLength = 0.1f;
    [SerializeField] private float _forwardRayCastHeightPercentage = 0.8f;

    private const float _speedToMove = 0.01f;
    private int _moveDirection = 0;
    private bool _isGrounded = false;
    private bool _jumpRequested = false;

    private Rigidbody2D _r2d;
    private BoxCollider2D _collider;
    private SpriteRenderer _spriteRdr;
    private Animator _animator;
    private float _initialGravityScale = 1.0f;

    //audio
    [SerializeField]
    public AudioClip jump;
    public AudioClip land;
    AudioSource audioSource;

    void Awake()
    {
        _r2d = GetComponent<Rigidbody2D>();
        _collider = GetComponent<BoxCollider2D>();
        _spriteRdr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();

        _initialGravityScale = _r2d.gravityScale;

        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        float horizontalMoveInput = Input.GetAxisRaw("Horizontal");
        float verticalMoveInput = Input.GetAxisRaw("Vertical");

        if ( (Mathf.Abs(horizontalMoveInput) > Mathf.Epsilon) )
        {
            _moveDirection = horizontalMoveInput > 0 ? 1 : -1;
        }
        else
        {
            if (_isGrounded || _r2d.velocity.magnitude < _speedToMove)
            {
                _moveDirection = 0;
            }
        }

        // Change Facing Direction
        if (Mathf.Abs(_moveDirection) > 0)
        {
            _spriteRdr.flipX = _moveDirection > 0;
        }

        // Jumping
        if (Input.GetButtonDown("Jump") && _isGrounded)
        {
            _jumpRequested = true;
            audioSource.PlayOneShot(jump, 0.7F);
        }

        _r2d.gravityScale = (_isGrounded) ? 0.0f : _initialGravityScale;

        _animator.SetBool("running", Mathf.Abs(_moveDirection) > 0);
        _animator.SetBool("jumping", !_isGrounded);
    }

    private bool BoxcastToPlatform()
    {
        Bounds colliderBounds = _collider.bounds;
        Vector3 groundCheckPos = _collider.bounds.min + new Vector3(colliderBounds.size.x * 0.5f, _footRayCastLength * 0.5f, 0.0f);
        RaycastHit2D[] raycasts = Physics2D.BoxCastAll(groundCheckPos, new Vector2(_collider.size.x * 0.5f, _footRayCastLength * 0.1f), 0f, Vector2.down, _footRayCastLength);

        for(int i = 0, size = raycasts.Length; i < size; i++)
        {
            if (raycasts[i].collider != _collider)
            {
                return true;
            }
        }
        return false;
    }

    private bool RaycastForward()
    {
        Vector2 direction = transform.right * (_spriteRdr.flipX ? 1 : -1);
        RaycastHit2D raycast = Physics2D.BoxCast(transform.position, new Vector2(0.001f, _collider.size.y * _forwardRayCastHeightPercentage), 
            0f, direction, _forwardRayCastLength - 0.001f, LayerMask.GetMask("Wall"));

        if (raycast.collider != null && raycast.collider != _collider)
        {
            return true;
        }
        return false;
    }

    void FixedUpdate()
    {
        Bounds colliderBounds = _collider.bounds;
        float colliderRadius = _collider.size.x * 0.5f;
        Vector3 groundCheckPos = _collider.bounds.min + new Vector3(colliderBounds.size.x * 0.5f, 0.0f, 0.0f);
        // Check if player is grounded
        colliderRadius *= 2;
        Collider2D[] colliders = Physics2D.OverlapBoxAll(groundCheckPos, new Vector2(colliderRadius, _footRayCastLength), 0f, LayerMask.GetMask("Default"));

        _isGrounded = false;

        float yVel = _r2d.velocity.y;
        if (_jumpRequested)
        {
            yVel = _jumpHeight;
            _jumpRequested = false;
        }
        else
        {
            bool raycastBoxDown = BoxcastToPlatform();
            if (colliders.Length > 0 && raycastBoxDown)
            {
                for (int i = 0; i < colliders.Length; i++)
                {
                    if (colliders[i] != _collider)
                    {
                        _isGrounded = true;
                        if (colliders[i].TryGetComponent<MovingPlatformPlayerInformer>(out var pController) && Mathf.Abs(yVel) < Mathf.Epsilon )
                        {
                            transform.position = transform.position + Vector3.up * (pController.FixedPositionDelta.y);
                        }
                        break;
                    }
                }
            }
        }

        bool fwdBlocked = RaycastForward();
        if (fwdBlocked)
        {
            _moveDirection = 0;
        }
        // Apply Movement Velocity
        _r2d.velocity = new Vector2((_moveDirection) * _maxSpeed, yVel);

        // Simple debug
        Vector3 direction = transform.right * (_spriteRdr.flipX ? 1 : -1);
        Debug.DrawLine(transform.position, transform.position + direction * 0.1f, !fwdBlocked ? Color.green : Color.red);
        Debug.DrawLine(groundCheckPos, groundCheckPos - new Vector3(0, _footRayCastLength * 0.5f, 0), _isGrounded ? Color.green : Color.red);
        Debug.DrawLine(groundCheckPos, groundCheckPos - new Vector3(colliderRadius * 0.5f, 0, 0), _isGrounded ? Color.green : Color.red);
    }
}