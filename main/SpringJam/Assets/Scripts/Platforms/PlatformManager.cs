﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformManager : MonoBehaviour
{
    [SerializeField] PlatformController[] _platforms;
    [SerializeField] PlatformController _ground;
    [SerializeField] PlantController[] _plants;
    [SerializeField] HeightCounter _heightCounter;
    [SerializeField] float _moveSpeed;
    [SerializeField] float _maxSpeed = .55f;
    [SerializeField] float _acceleration = .015f;
    [ReadOnly] public float _currentSpeed;
    [SerializeField] float _plantSpeed = .333f;
    private float _spawnDistance = 3.5f;
    int _nextPlatform = 0;

    private void Start()
    {
        ChangeSpeed(_moveSpeed);
        _currentSpeed = _moveSpeed;

        foreach (PlantController plant in _plants)
        {
            plant.GrowthFactor = _plantSpeed;
        }
    }

    private void ChangeSpeed(float moveSpeed)
    {
        foreach (PlatformController platform in _platforms)
        {
            platform.MoveSpeed = moveSpeed;
        }
        foreach (PlantController plant in _plants)
        {
            plant.MoveSpeed = moveSpeed;
        }
        _ground.MoveSpeed = moveSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if (_platforms[_nextPlatform].transform.position.y <= -_spawnDistance)
        {
            RespawnPlatform();
            if (_currentSpeed < 5.48f)
            {
                _currentSpeed = Mathf.Clamp(_currentSpeed + _acceleration, 0, _maxSpeed);
                ChangeSpeed(_currentSpeed);
            }
        }
        if(_ground.IsActive && _ground.transform.position.y <= -_spawnDistance)
        {
            _ground.IsActive = false;
        }
    }

    internal void StartLeaves()
    {
        foreach (PlantController plant in _plants)
        {
            plant.IsActive = true;
        }
    }

    private void RespawnPlatform()
    {
        int top = _nextPlatform - 1 > -1 ? _nextPlatform - 1 : _platforms.Length - 1;
        _platforms[_nextPlatform].Respawn(_platforms[top], _spawnDistance);
        _nextPlatform = _nextPlatform < _platforms.Length - 1 ? _nextPlatform + 1 : 0;
    }

    public IEnumerator StartMovement()
    {
        yield return new WaitForSeconds(2f);
        foreach(PlatformController platform in _platforms)
        {
            platform.IsActive = true;
        }
        foreach (PlantController plant in _plants)
        {
            plant.ScrollActive = true;
        }
        _ground.IsActive = true;
        _heightCounter.IsActive = true;
    }

    public void Reset()
    {
        _nextPlatform = 0;
        _currentSpeed = _moveSpeed;
        ChangeSpeed(_moveSpeed);
        for (int i=0; i<_platforms.Length; i++)
        {
            _platforms[i].IsActive = false;
            if (i <= 2)
            {
                _platforms[i].Reset();
            }
            else
            {
                _platforms[i].Reset(_platforms[i - 1]);
            }
        }
        foreach(PlantController plant in _plants)
        {
            plant.IsActive = false;
            plant.ScrollActive = false;
            plant.Reset();
        }
        _ground.IsActive = false;
        _ground.Reset();
    }
}
