﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeafCollision : MonoBehaviour
{
    [SerializeField]
    public PlantController plantController;
    [SerializeField]
    private Animator _animator;
    [SerializeField] AudioManager _audioManager;
    private EdgeCollider2D _collider;

    public float reGrowDelay = 3;
    private float _initialRegrowDelay = 0;

    public bool occupied = false;
    private bool resumingGrow = false;

    private void Awake()
    {
        _initialRegrowDelay = reGrowDelay;
        _collider = GetComponent<EdgeCollider2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 size = _collider.points[1] - _collider.points[0];
        size.y = 0.01f;
        RaycastHit2D boxHit = Physics2D.BoxCast(transform.position, size, 0f, Vector2.up, 0.1f, LayerMask.GetMask("Player"));
        if (boxHit.collider != null)
        {
            plantController.occupied = true;
            occupied = true;
            reGrowDelay = _initialRegrowDelay;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        resumingGrow = true;
        occupied = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (resumingGrow)
        {
            reGrowDelay = reGrowDelay - 1 * Time.deltaTime;
            if (reGrowDelay <= 0)
            {
                _animator.SetTrigger("Shake");
                _audioManager.PlayPlantShake();
                plantController.occupied = occupied;
                resumingGrow = false;
                reGrowDelay = _initialRegrowDelay;
            }
        }
    }
}
