﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtendStem : MonoBehaviour
{
    [SerializeField]
    private float heightIncrease = 0.2f;
    [SerializeField] private PlantController plantController;

    private SpriteRenderer _spriteRender;

    private void Awake()
    {
        _spriteRender = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!plantController.occupied)
        {
            Vector2 currentSize = _spriteRender.size;
            Vector3 currentPos = gameObject.transform.position;

            _spriteRender.size = new Vector2(x: currentSize.x, y: currentSize.y + heightIncrease);
            gameObject.transform.position = new Vector3(x: currentPos.x, y: currentPos.y - (heightIncrease / 2), z: currentPos.z);
        }
    }
}
