﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantController : MonoBehaviour
{
    private int _column;
    public int Column => _column;
    public bool occupied = false;

    private float _moveSpeed = .5f;
    public float MoveSpeed { set { _moveSpeed = value; } }
    private float _growthFactor = .333f;
    public float GrowthFactor { set { _growthFactor = value; } }

    [SerializeField] private float _columnChangeChance = .76f;

    [SerializeField]
    private bool _isActive = false;
    public bool IsActive { get { return _isActive; } set { _isActive = value; } }
    public bool ScrollActive;

    private Vector3 _ogPos;

    private void Start()
    {
        _column = (int)(transform.position.x / .65f);
        _ogPos = transform.position;
        //_isActive = false;
        //ScrollActive = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!_isActive)
            return;
        if (!occupied)
        {
            Grow();
        }
        else if (ScrollActive)
        {
            MoveDown();
        }
    }

    public void Grow()
    {
        if (ScrollActive)
            transform.position = new Vector2(_column * .65f, transform.position.y + (_moveSpeed * Time.deltaTime * _growthFactor));
        else
            transform.position = new Vector2(_column * .65f, transform.position.y + (_moveSpeed * Time.deltaTime));
    }

    public void MoveDown()
    {
        transform.position = new Vector2(_column * .65f, transform.position.y - (_moveSpeed * Time.deltaTime));
    }

    public void Reset()
    {
        transform.position = _ogPos;
        _column = (int)(transform.position.x / .65f);
        occupied = false;
    }

    public void Respawn(PlantController topPlatform, float spawnDistance)
    {
        float rand = UnityEngine.Random.value;
        if (Math.Abs(topPlatform.Column) == 2)
        {
            if (rand < _columnChangeChance + 2)
                _column = 0;
            else
                _column = topPlatform.Column;
        }
        else
        {
            if (rand < _columnChangeChance / 2)
                _column = -2;
            else if (rand < _columnChangeChance)
                _column = 2;
            else
                _column = 0;
        }

        transform.position = new Vector2(_column * .65f, spawnDistance);
    }
}
