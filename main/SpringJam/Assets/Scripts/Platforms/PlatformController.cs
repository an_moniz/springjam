﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour
{
    private int _column;
    public int Column => _column;

    [SerializeField]
    private bool _isActive = false;
    public bool IsActive { get { return _isActive; } set { _isActive = value; } }

    private float _moveSpeed = .5f;
    public float MoveSpeed { set { _moveSpeed = value; } }
    [SerializeField] private float _columnChangeChance = .76f;

    private Vector3 _ogPos;

    private void Start()
    {
        _column = (int)(transform.position.x / .65f);
        _ogPos = transform.position;
        //_isActive = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (_isActive)
            MoveDown();
    }

    public void MoveDown()
    {
        transform.position = new Vector2(_column * .65f, transform.position.y - (_moveSpeed * Time.fixedDeltaTime));
    }

    public void Reset()
    {
        transform.position = _ogPos;
        _column = (int)(transform.position.x / .65f);
    }

    public void Reset(PlatformController lastPlatform)
    {
        float rand = UnityEngine.Random.value;
        if (Math.Abs(lastPlatform.Column) == 2)
        {
            if (rand < _columnChangeChance + .18f)
                _column = 0;
            else
                _column = lastPlatform.Column;
        }
        else
        {
            if (rand < _columnChangeChance / 2)
                _column = -2;
            else if (rand < _columnChangeChance)
                _column = 2;
            else
                _column = 0;
        }
        transform.position = new Vector2(_column * .65f, _ogPos.y);
    }

    public void Respawn(PlatformController topPlatform, float spawnDistance)
    {
        float rand = UnityEngine.Random.value;
        if (Math.Abs(topPlatform.Column) == 2)
        {
            if (rand < _columnChangeChance + .18f)
                _column = 0;
            else
                _column = topPlatform.Column;
        }           
        else
        {
            if (rand < _columnChangeChance / 2)
                _column = -2;
            else if (rand < _columnChangeChance)
                _column = 2;
            else
                _column = 0;
        }
        gameObject.SetActive(false);
        transform.position = new Vector2(_column * .65f, spawnDistance);
        gameObject.SetActive(true);
    }
}
