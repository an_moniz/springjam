﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowIfNotTouchingPlayer : MonoBehaviour
{
    [SerializeField]
    private float _speed;

    private bool _playerOnMe = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!_playerOnMe)
            transform.position += _speed * Vector3.up * Time.deltaTime;
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        _playerOnMe = (collision.transform.CompareTag("Player"));
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        _playerOnMe = !(collision.transform.CompareTag("Player"));
    }
}
