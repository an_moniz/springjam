﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

public enum BindingVisibility
{
    /// <summary>
    /// Will not be accessible through GetBinding
    /// </summary>
    Hidden,
    /// <summary>
    /// Will be accessible only by class registered in the context
    /// </summary>
    Context,
    /// <summary>
    /// Will be accessible only by class registered in the the context and all child context
    /// </summary>
    Children
}

public interface IBindingResolver
{
    bool HasBinding<T>(bool checkParents = true) where T : class;
    T GetBinding<T>(bool checkParents = true) where T : class;
}

public abstract class Context
{
    /// <summary>
    /// Binding entry that specifies visibility level
    /// </summary>
    private class Binding
    {
        /// <summary>
        /// The bound resources
        /// </summary>
        public readonly object Value;

        /// <summary>
        /// The ITick (if inherit)
        /// </summary>
        public readonly ITick Tick;

        /// <summary>
        /// If true, the children can access this bound value
        /// </summary>
        public readonly BindingVisibility Visibility;

        public bool IsInitialized { get; set; }

        public Binding(object value, BindingVisibility visibility)
        {
            Value = value;
            Visibility = visibility;

            Tick = null;
            if (value is ITick tick)
                Tick = tick;
        }
    }

    protected Context ParentContext { get; private set; }

    protected readonly List<Context> ChildrenContexts = new List<Context>();
    private List<Context> _childrenContextsToRemove = new List<Context>();

    /// <summary>
    /// The internal map of bindings associated to this context
    /// </summary>
    private readonly Dictionary<Type, Binding> _bindings = new Dictionary<Type, Binding>();

    protected void Dispose()
    {
        // Dispose All children
        foreach (var child in ChildrenContexts)
        {
            child.Dispose();
        }
        ChildrenContexts.Clear();

        // Dispose All bindings
        foreach (var entry in _bindings.Values)
        {
            if (entry.Value is IDisposable disposable)
                disposable.Dispose();
        }
        _bindings.Clear();

        OnContextDispose();
    }

    protected async Task InitializeBindings()
    {
        InstallBindings();

        foreach (var entry in _bindings.Values)
        {
            if (entry.Value is IInitialize initialize)
                initialize.Initialize();

            entry.IsInitialized = true;
        }
    }

    protected void Update()
    {
        for (var i = 0; i < ChildrenContexts.Count; i++)
        {
            ChildrenContexts[i].Update();
        }

        foreach (var entry in _bindings.Values)
        {
            if (entry.IsInitialized)
                entry.Tick?.Tick();
        }

        // Process the delayed operations
        ProcessChildrenContextsToRemove();
    }

    protected abstract void InstallBindings();

    protected virtual void Initialize() { }
    protected virtual void OnContextDispose() { }

    /// <summary>
    /// Create and add a child context of the given type to the ChildrenContexts list. Context types are unique within children lists.
    /// </summary>
    /// <typeparam name="T">Type of context to be childed to this context.</typeparam>
    /// <returns>Newly added child or null if type already exists in ChildrenContexts list.</returns>
    protected async Task<T> AddChildContext<T>() where T : Context, new()
    {
        if (ChildrenContexts.Any(c => c is T))
        {
            return null;
        }

        var childContext = new T();

        await AddChildContext(childContext);

        return childContext;
    }

    protected async Task AddChildContext<T>(T childContext) where T : Context
    {
        ChildrenContexts.Add(childContext);

        childContext.ParentContext = this;

        await childContext.InitializeBindings();
        childContext.Initialize();
    }

    /// <summary>
    /// Remove a child context of the given type from the ChildrenContexts list. 
    /// </summary>
    /// <typeparam name="T">Type of context to be removed.</typeparam>
    /// <returns>True if child was found and disposed. False if no child of type was found.</returns>
    public bool RemoveChildContext<T>() where T : Context
    {
        var foundChild = ChildrenContexts.FirstOrDefault(c => c is T);
        if (foundChild == null)
        {
            return false;
        }
        _childrenContextsToRemove.Add(foundChild);
        return true;
    }

    public bool RemoveChildContext<T>(T childContext) where T : Context
    {
        var foundChild = ChildrenContexts.FirstOrDefault(c => c == childContext);
        if (foundChild == null)
        {
            return false;
        }
        _childrenContextsToRemove.Add(childContext);
        return true;
    }

    private void ProcessChildrenContextsToRemove()
    {
        foreach (var context in _childrenContextsToRemove)
        {
            if (ChildrenContexts.Remove(context))
            {
                context.Dispose();
            }
        }

        _childrenContextsToRemove.Clear();
    }

    /// <summary>
    /// Check if a binding exists for the provided type
    /// </summary>
    /// <param name="checkParents">Should the check run on parent contexts</param>
    /// <typeparam name="T">The binding type to check for</typeparam>
    /// <returns>True if a binding is found, false otherwise</returns>
    public bool HasBinding<T>(bool checkParents = true) where T : class => HasBindingInternal<T>(checkParents, false);

    /// <summary>
    /// Check if a binding exists for the provided type
    /// </summary>
    /// <param name="checkParents">Should the check run on parent contexts</param>
    /// <param name="fromChild">If true, this method was called from a child context</param>
    /// <typeparam name="T">The binding type to check</typeparam>
    /// <returns>True if a binding is found, false otherwise</returns>
    private bool HasBindingInternal<T>(bool checkParents, bool fromChild)
    {
        if (_bindings.TryGetValue(typeof(T), out var binding))
        {
            if (binding.Visibility == BindingVisibility.Hidden)
                return false;

            if (binding.Visibility == BindingVisibility.Context)
                return !fromChild && binding.Value != null;

            return binding.Value != null;
        }

        if (checkParents && ParentContext != null)
        {
            return ParentContext.HasBindingInternal<T>(true, true);
        }

        return false;
    }

    /// <summary>
    /// Gets the object bound on the provided type; throws if the binding doesn't exist
    /// </summary>
    /// <param name="checkParents">Should parent contexts be checked</param>
    /// <typeparam name="T">The binding type to get</typeparam>
    /// <returns>The object bound to the provided type</returns>
    // [NotNull]
    public T GetBinding<T>(bool checkParents = true) where T : class => GetBindingInternal<T>(checkParents, false);

    /// <summary>
    /// Gets the object bound on the provided type; throws if the binding doesn't exist
    /// </summary>
    /// <param name="checkParents">Should parent contexts be checked</param>
    /// <param name="fromChild">If true, this method was called from a child context</param>
    /// <typeparam name="T">The binding type to get</typeparam>
    /// <returns>The object bound to the provided type</returns>
    // [NotNull]
    private T GetBindingInternal<T>(bool checkParents, bool fromChild) where T : class
    {
        if (_bindings.TryGetValue(typeof(T), out var binding))
        {
            if (binding.Visibility == BindingVisibility.Hidden)
                throw new Exception($"The requested binding of type '{typeof(T).Name}' exists, but can't be accessed.");

            if (!fromChild || binding.Visibility == BindingVisibility.Children)
                return (T)binding.Value;

            throw new Exception($"The requested binding of type '{typeof(T).Name}' exists, but can't be accessed by a child context");
        }

        if (checkParents && ParentContext != null)
        {
            return ParentContext.GetBindingInternal<T>(true, true);
        }

        // This point can only be reached by the top parent because of
        // the previous recursion, indicating failure
        throw new KeyNotFoundException($"No binding registered for type '{typeof(T).Name}'");
    }

    /// <summary>
    /// Bind an object to the specified type
    /// </summary>
    /// <param name="binding">The object to bind</param>
    /// <param name="visibility">Visibility level of the binding <see cref="BindingVisibility"/></param>
    /// <typeparam name="T">The type to bind the object to</typeparam>
    protected void RegisterBinding<T>( /*[NotNull]*/ T binding, BindingVisibility visibility = BindingVisibility.Children) where T : class
    {
        if (binding == null)
        {
            throw new ArgumentException("Can't register a null binding, if you wish to seal a binding, use `SealBinding<T>()`");
        }

        _bindings.Add(typeof(T), new Binding(binding, visibility));
    }

    /// <summary>
    /// Seals access to the specified type binding; children contexts won't have access anymore
    /// </summary>
    /// <typeparam name="T">The type of binding to seal</typeparam>
    protected void SealBinding<T>() where T : class
    {
        var type = typeof(T);
        if (_bindings.TryGetValue(type, out var binding) && binding.Value != null)
        {
            Debug.LogWarning($"The local binding for type '{type.Name}''s access level has been changed, consider binding it with 'BindingVisibility.Context'");
            _bindings[type] = new Binding(binding.Value, BindingVisibility.Context);
        }
        else if (ParentContext.HasBinding<T>())
        {
            _bindings.Add(type, new Binding(ParentContext.GetBindingInternal<T>(true, true), BindingVisibility.Context));
        }
        else
        {
            throw new Exception($"Cannot seal a binding of type '{type.Name}' as it doesn't exist");
        }
    }

    /// <summary>
    /// Unbinds the object bound to the specified type
    /// </summary>
    /// <typeparam name="T">The type to unbind</typeparam>
    /// <returns>True if an object was unbound, false otherwise</returns>
    protected bool UnregisterBinding<T>() where T : class
    {
        return _bindings.Remove(typeof(T));
    }

    protected void UnloadSelf()
    {
        if (ParentContext != null)
        {
            ParentContext.RemoveChildContext(this);
        }
        else
        {
            Dispose();
        }
    }
}
