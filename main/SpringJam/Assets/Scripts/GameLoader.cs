﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLoader : MonoBehaviour
{
    [SerializeField] PlatformManager _platformManager;
    [SerializeField] GameObject _startScreen;
    [SerializeField] GameObject _gameOverScreen;
    [SerializeField] GameObject _scoreCanvas;
    [SerializeField] AudioManager _audioManager;
    [SerializeField] RandomToolTip _toolTip;
    
    [SerializeField] Animator _bgBlue;

    [SerializeField] GameObject _player;
    Vector3 _playerStartPos = new Vector3(-1, -1.885f, 0);


    GameState _gameState = GameState.Title;
    enum GameState {Title, Loaded, Play, GameOver};

    public void LoadGame()
    {
        _bgBlue.SetBool("fadeout", false);
        _platformManager.gameObject.SetActive(true);
        _platformManager.StartLeaves();
        _startScreen.SetActive(false);
        _gameOverScreen.SetActive(false);
        _scoreCanvas.SetActive(true);
        _scoreCanvas.GetComponentInChildren<HeightCounter>().Reset();
        _gameState = GameState.Loaded;
        _player.SetActive(true);
        _player.transform.position = _playerStartPos;

        //play bg music
        _audioManager.PlayMusic();
    }

    public void StartScroll()
    {
        _gameState = GameState.Play;
        StartCoroutine(_platformManager.StartMovement());
    }

    public void GameOver()
    {
        _bgBlue.SetBool("fadeout", true);
        _platformManager.Reset();
        _platformManager.gameObject.SetActive(false);
        _gameOverScreen.SetActive(true);
        _scoreCanvas.SetActive(false);
        _gameState = GameState.GameOver;
        _toolTip.GenerateNewToolTip();

        _player.transform.position = _playerStartPos;
        _player.SetActive(false);

        ScoreOnGameOver();

        //stop bg music
        _audioManager.StopMusic();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            switch (_gameState)
            {
                case GameState.Title:
                case GameState.GameOver:
                    LoadGame();
                    break;
                case GameState.Loaded:
                    StartScroll();
                    break;
            }
        }
        if (_player.transform.position.y <= -3.1f)
        {
            GameOver();
        }
    }

    void ScoreOnGameOver()
    {
        float final_height;
        string message;

        final_height = _scoreCanvas.GetComponentInChildren<HeightCounter>().GetHeight();

        message = "Final Score \n" + Mathf.Floor(final_height).ToString() + "\n\n" + "Press Space to Retry";
        Debug.Log(message);

        _gameOverScreen.GetComponentInChildren<StartTextUpdater>().update_text(message);
    }
}
