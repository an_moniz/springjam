﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    //audio
    [SerializeField]
    public AudioClip loop1;
    public AudioClip loop2;
    public AudioClip death;
    public AudioClip grow;

    AudioSource audioSource;

    public void PlayMusic()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.PlayOneShot(loop1, 1f);
        audioSource.clip = loop2;
        audioSource.PlayDelayed(loop1.length);
    }

    public void StopMusic()
    {
        audioSource.Stop();
        audioSource.PlayOneShot(death, 1f);
    }

    public void PlayPlantShake()
    {
        audioSource.PlayOneShot(grow, 1.15f);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
