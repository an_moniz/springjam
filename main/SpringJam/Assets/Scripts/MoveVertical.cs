﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveVertical : MonoBehaviour
{
    [SerializeField]
    private float _yMin;
    [SerializeField]
    private float _yMax;

    [SerializeField]
    private float _speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position -= _speed * Vector3.down * Time.deltaTime;
        if (transform.position.y < _yMin)
        {
            transform.position = new Vector3(transform.position.x, _yMax - 0.5f, transform.position.z);
        }
        else if (transform.position.y > _yMax)
        {
            transform.position = new Vector3(transform.position.x, _yMin + 0.5f, transform.position.z);
        }
    }
}
