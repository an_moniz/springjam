﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour 
{
    [SerializeField] private GameObject _camera;

    [SerializeField] private float _scrollSpeed;

    bool _levelLoaded = false;
    // Start is called before the first frame update
    void Start()
    {
        _levelLoaded = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (_levelLoaded)
            _camera.transform.position = new Vector2(_camera.transform.position.x, _camera.transform.position.y + _scrollSpeed);
    }
}
