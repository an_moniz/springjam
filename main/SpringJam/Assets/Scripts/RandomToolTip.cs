﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomToolTip : MonoBehaviour
{

    [SerializeField] private string[] _textTip;
    [SerializeField] private TextAsset _textTipFile;
    [SerializeField] private TMPro.TextMeshProUGUI _randomTip;
        

    // Start is called before the first frame update
    void Awake()
    {
       _textTip = _textTipFile.text.Split("\n".ToCharArray());
        _randomTip.text = _textTip[Random.Range(0,_textTip.Length -1)];
    }

    public void GenerateNewToolTip()
    {
        _randomTip.text = _textTip[Random.Range(0, _textTip.Length - 1)];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
