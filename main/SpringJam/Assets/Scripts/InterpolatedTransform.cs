﻿using System;
using UnityEngine;

[RequireComponent(typeof(InterpolatedTransformUpdater))]
public class InterpolatedTransform : MonoBehaviour
{
    private struct TransformData
    {
        public Vector3 position;
        public Quaternion rotation;
        public Vector3 scale;

        public TransformData(Vector3 pos, Quaternion rot, Vector3 scale)
        {
            position = pos;
            rotation = rot;
            this.scale = scale;
        }
    }

    private Transform _transform;
    private TransformData[] _lastTransforms;
    private int _newTransformIndex;

    public Vector3 FixedPositionDelta => _lastTransforms[_newTransformIndex].position - _lastTransforms[OldTransformIndex()].position;

    private void Awake()
    {
        _transform = transform;
    }

    void OnEnable()
    {
        ForgetPreviousTransforms();
    }

    private void ForgetPreviousTransforms()
    {
        _lastTransforms = new TransformData[2];
        TransformData t = new TransformData(
                            _transform.localPosition, _transform.localRotation, _transform.localScale);
        _lastTransforms[0] = t;
        _lastTransforms[1] = t;
        _newTransformIndex = 0;
    }

    public void FixedUpdate()
    {
        TransformData newestTransform = _lastTransforms[_newTransformIndex];
        _transform.localPosition = newestTransform.position;
        _transform.localRotation = newestTransform.rotation;
        _transform.localScale = newestTransform.scale;
    }

    public void LateFixedUpdate()
    {
        _newTransformIndex = OldTransformIndex();
        _lastTransforms[_newTransformIndex] = new TransformData(_transform.localPosition, _transform.localRotation, _transform.localScale);
    }

    private int OldTransformIndex()
    {
        return (_newTransformIndex == 0 ? 1 : 0);
    }
}
